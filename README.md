<p align="center">
    <img src="https://res.cloudinary.com/dgzk4utc6/image/upload/v1589334424/logo.png" width="300">
</p>

<p align="center">
    <a href="https://gitlab.com/sfoon/framework">
        <img src="https://img.shields.io/badge/License-MIT-yellow.svg">
        <img src="http://ForTheBadge.com/images/badges/built-by-developers.svg">
        <img src="http://ForTheBadge.com/images/badges/uses-git.svg">
    </a>
</p>

## About Sfoon

This repository contains the core code of the Sfoon E-Commerce Framework that is based on Laravel.

## Requirements

The minimum Laravel version required is 5.8.

## Installation

The Sfoon framework can be installed by using Composer ```require``` command in your terminal:

```bash
composer require sfoon/framework
```